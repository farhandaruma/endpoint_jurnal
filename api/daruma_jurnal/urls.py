from django.urls import path
from .views import ListJurnal_JobsView, jurnal_job, view_response, ListJurnal_JobsView_Live, view_response_live

urlpatterns = [
    path('jobs/', ListJurnal_JobsView.as_view(), name="view-jobs-all"),
    path('live/', ListJurnal_JobsView_Live.as_view(), name="view-live-jobs-all"),
    path('view-response/', view_response, name="view-response"),
    path('view-response-live/', view_response_live, name="view-response-live"),
]

