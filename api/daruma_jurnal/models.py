from django.db import models
from django.utils.timezone import now


# Create your models here.
class Jurnal_Jobs(models.Model):

    # id
    job_id = models.AutoField(primary_key=True)

    # input_data
    data_input = models.TextField(null=False)

    # status job
    status = models.CharField(max_length=255, null=False, default="pending")

    # response
    response = models.TextField(default="-", null=False)

    # created_at
    created_at = models.DateTimeField(blank=True, auto_now_add=True)

    def __str__(self):
        return f"{self.job_id} status:{self.status}  input_data: {self.data_input} created_at: {self.created_at}"


class Jurnal_Jobs_Live(models.Model):

    # id
    job_id = models.AutoField(primary_key=True)

    # input_data
    data_input = models.TextField(null=False)

    # status job
    status = models.CharField(max_length=255, null=False, default="pending")

    # response
    response = models.TextField(default="-", null=False)

    # created_at
    created_at = models.DateTimeField(blank=True, auto_now_add=True)

    def __str__(self):
        return f"{self.job_id} status:{self.status}  input_data: {self.data_input} created_at: {self.created_at}"
