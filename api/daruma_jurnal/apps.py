from django.apps import AppConfig


class DarumaJurnalConfig(AppConfig):
    name = 'daruma_jurnal'
